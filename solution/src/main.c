#include "image.h"
#include "bmp.h"
#include "transform.h"
#include <stdio.h>

static void close_files(FILE *a, FILE *b){
    if(!a) fclose(a);
    if(!b) fclose(b);
}

static void destroy_images(struct image *a, struct image *b){
    destroy_image(a);
    destroy_image(b);
}

void usage(){
	fprintf(stderr, "Usage: ./image-transformer <source-image><transformed-image>\n");
}

int main( int argc, char* argv[] ) {
    FILE *fin = NULL;
    FILE *fout = NULL;
    if(argc != 3){
    	usage();
    	return 1;
    }
    if((fin = fopen(argv[1], "rb")) == NULL || (fout = fopen(argv[2], "wb")) == NULL)
    {
        close_files(fin, fout);
    	fprintf(stderr, "Cannot open this files\n");
    	return 1;
    }else fprintf(stdout, "files are ok\n");

    struct image img;
    if(from_bmp(fin, &img) != READ_OK){
        close_files(fin, fout);
        destroy_image(&img);
        fprintf(stderr, "Cannot read bmp\n");
        return 1;
    } else fprintf(stdout, "bmp readed\n");

    struct image rotated = rotate(img);

    if(rotated.data == NULL){
        destroy_images(&img, &rotated);
        close_files(fin, fout);
        fprintf(stderr, "Cannot rotate image\n");
        return 1;
    }

    if(to_bmp(fout, &rotated) != WRITE_OK){
        destroy_images(&img, &rotated);
        close_files(fin, fout);
        fprintf(stderr, "Cannot write bmp\n");
        return 1;
    } else fprintf(stdout, "File written\n");

    destroy_images(&img, &rotated);

    close_files(fin, fout);

    return 0;
}
