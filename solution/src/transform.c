#include "transform.h"
#include <stdlib.h>

struct image rotate( struct image const source ) {
    struct image new_img = create_image(source.height, source.width);
    if(new_img.data == NULL) return new_img;

    for (int32_t i = 0; i < source.height; i++) {
        for (int j = 0; j < source.width; j++) {
            const struct pixel color = source.data[i*source.width + j];
            uint32_t target_x = j;
            uint32_t target_y = source.height-1-i;
            new_img.data[target_x*new_img.width + target_y] = color;
        }
    }
    return new_img;
}
