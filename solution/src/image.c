#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_image(uint32_t width, uint32_t height){
	return (struct image) {.width = width, .height = height, .data = malloc(sizeof(struct pixel)*width*height)};
}

void destroy_image(const struct image *img){
	if(img == NULL) return;
	free(img->data);
}
