#include "image.h"
#include "bmp.h"
#include <inttypes.h>
#include <stdlib.h>

#define MAGIC_BMP_TYPE 19778
#define MAGIC_OFF_BITS 54
#define MAGIC_BI_SIZE 40
#define MAGIC_BIT_COUNT 24


static uint32_t bytes_padding(const uint32_t width) {
    if (width % 4 == 0) {
        return 0;
    }
    return 4 - ((width * sizeof(struct pixel)) % 4);
}

static size_t image_size(const struct image *image) {
    return (image->width * sizeof(struct pixel) + bytes_padding(image->width)) * image->height;
}

static size_t file_size(const struct image *image) {
    return image_size(image) + sizeof(struct bmp_header);
}

static struct bmp_header generate_bmp_header(const struct image *image) {
    return (struct bmp_header) {
            .bfType = MAGIC_BMP_TYPE,
            .bfileSize = file_size(image),
            .bfReserved = 0,
            .bOffBits = MAGIC_OFF_BITS,
            .biSize = MAGIC_BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = MAGIC_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = image_size(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header bmp_header = {0};
    if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);
    if(!image->data){
        return READ_ALLOCATION_FAILURE;
    }

    const size_t padding = bytes_padding(image->width);

    for (size_t i = 0; i < image->height; ++i) {
        size_t res = fread(&image->data[image->width*i], sizeof(struct pixel), image->width, in);
        if(res != image->width){
            return READ_BAD_IMAGE_DATA;
        }
        if(fseek(in, padding, SEEK_CUR)){  
            return READ_BAD_IMAGE_DATA;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *image) {
    struct bmp_header bmp_header = generate_bmp_header(image);

    if (!fwrite(&bmp_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    if(fseek(out, bmp_header.bOffBits, SEEK_SET) != 0){
        return WRITE_ERROR;
    }

    const uint8_t zero = 0;

    const size_t padding = bytes_padding(image->width);

    if (image->data != NULL) {
        for (size_t i = 0; i < image->height; ++i) {
            fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out);
            for (size_t j = 0; j < padding; ++j) {
                if(!fwrite(&zero, 1, 1, out)){
                    return WRITE_ERROR;
                }
            }
        }
    } else {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
